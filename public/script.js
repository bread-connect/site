function spin(){
    var elem = document.querySelectorAll('.carousel')[0];
    var instance = M.Carousel.getInstance(elem);
    instance.next();
}

function dbAPI(type) {
    validTypes = ['titles', 'media', 'desc', 'dl']
    if(!validTypes.includes(type)) return Error;
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            return use(xmlHttp.responseText, type)
    }
    xmlHttp.open("GET", "https://bakery-gg.herokuapp.com/" + type, true);
    xmlHttp.send(null);
}

function use(data, type) {
    var element = document.getElementById('data');
    element.innerHTML += data
    for(var i = 0; i<50; i++){
        data = data.replace('[','');
        data = data.replace(']', '');
        data = data.replace('"', '');
        data = data.replace('\\n', '<br>')
    }
    if(type == 'media') {
        var i = 0
        data = data.split(',');
        data.forEach(element => {
            gen(i);
            if(element.search('.mp4') != -1){
                var elem = document.createElement("video");
                elem.setAttribute('src', element);
                elem.setAttribute('controls', 'true');
                document.getElementById(i + '-img').appendChild(elem);
                elem.classList.add('responsive-video');
            } else {
                var elem = document.createElement("img");
                elem.setAttribute('src', element);
                document.getElementById(i + '-img').appendChild(elem);
                elem.classList.add('responsive-img')
            }
            i++;
        });
        dbAPI('titles');
    } else if(type == 'titles') {
        var i = 0
        data = data.split(',');
        data.forEach(element => {
            document.getElementById(i + '-title').innerHTML = element;
            i++
        })
        dbAPI('desc');
    } else if(type == 'desc') {
        var i = 0
        data = data.split(',');
        data.forEach(element => {
            document.getElementById(i + '-desc').innerHTML = element;
            i++
        })
        dbAPI('dl');
    } else if(type == 'dl') {
        var i = 0;
        data = data.split(',');
        data.forEach(element => {
            var elem = document.createElement('a');
            elem.setAttribute('href', element)
            document.getElementById(i + '-act').appendChild(elem);
            elem.innerHTML = 'Download';
            i++;
        })
    }
    setTimeout(() => {
        var toastElement = document.querySelector('.toast');
        var toastInstance = M.Toast.getInstance(toastElement);
        toastInstance.dismiss();
    }, 3000);
}

function gen(num) {
    var elem = document.createElement('div')
    document.getElementById('cards').appendChild(elem)
    elem.classList.add('col');
    var card = document.createElement('div')
    elem.appendChild(card)
    card.classList.add('card', 'blue-grey', 'darken-3', 'white-text', 'hoverable')
    var elem = document.createElement('div')
    card.appendChild(elem)
    elem.classList.add('card-img')
    elem.id = num + '-img'
    var elem = document.createElement('div')
    card.appendChild(elem)
    elem.classList.add('card-content')
    var elem2 = document.createElement('span')
    elem.appendChild(elem2)
    elem2.classList.add('card-title')
    elem2.id = num + '-title'
    var elem2 = document.createElement('p')
    elem.appendChild(elem2)
    elem2.id = num + '-desc'
    var elem = document.createElement('div');
    card.appendChild(elem)
    elem.classList.add('card-action', 'right-align')
    elem.id = num + '-act'
}